import {
  Router
} from 'restify-router'
import {
  routes
} from './../config/route.js'

const routerInstance = new Router();

routes.forEach((x) => {
  routerInstance[x.method](x.path, x.rhandler)
})

export {
  routerInstance
}