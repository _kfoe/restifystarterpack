import foo_handler from '../../router/handler/foo/generic.js'

const routes = [{
  path: '/',
  method: 'get',
  rhandler: foo_handler.bar
}]

export {
	routes
}