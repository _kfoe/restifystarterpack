global.$q = require('q')

import restify from 'restify'
import logger from 'morgan'

import {
  routerInstance
} from './router/routes'

import {
  foo
} from './middlewares/foo'

var server = restify.createServer();

server.opts("/.*/", function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:8080");
  res.header("Access-Control-Allow-Methods", req.header("Access-Control-Request-Method"));
  res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
  res.send(200);
  return next();
});

server.use((req, res, next) => foo(req, res, next))
server.use(logger('dev'));
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser({
  mapParams: true
}));
server.use(restify.plugins.bodyParser({
  mapParams: true
}));

server.get('/resource\/.*/', restify.plugins.serveStatic({
  'directory': './public',
  'default': 'index.html'
}));

//esempio di rotta con async/await
server.get('/random', async (req, res, next) => {
  try {
    let response = await require('axios').get('https://api.icndb.com/jokes/random')
    res.send(response.data)
  } catch (e) {
    next(e)
  }
})

routerInstance.applyRoutes(server);

server.listen(4113, function() {
  console.log('%s listening at %s', server.name, server.url);
});